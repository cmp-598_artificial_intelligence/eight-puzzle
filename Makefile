all: main
main: main.o bfs.o
	gcc -o main main.o bfs.o
bfs.o: src/bfs.c
	gcc -o bfs.o src/bfs.c
main.o: main.c src/bfs.h
	gcc -o main.o main.c src/bfs.h
clean:
	rm -rf *.o main